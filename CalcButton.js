import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button } from 'react-native-elements';

export default class CalcButton extends React.Component {

  _onPressButton(button) {
    Alert.alert('You tapped the button! ' + button)
  }

  render() {
    return (
      <View style={styles.buttonContainer}>
        <Button
            fontSize={20}
            containerViewStyle={styles.buttonContainerView}
            onPress={this.props.callback}
            title={this.props.label}
            buttonStyle={{
              backgroundColor: "#36578c",
              width: '100%',
              borderWidth: 0,
              borderRadius: 15
            }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexGrow: 1,
  },
  buttonContainerView: {
    width: '90%',
  }
});
