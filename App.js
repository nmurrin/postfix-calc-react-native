import React from 'react';
import { Alert, StyleSheet, Text, View, Dimensions } from 'react-native';
import CalcButton from './CalcButton';
import { Button, Overlay } from 'react-native-elements';
import Platform from './Platform'
import BigNumber from 'bignumber.js';

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      calcStack: '',
      stack: new Array(),
      input: ' ',
      output: '',
      errorMessage: false,
      orientation: Platform.isPortrait() ? 'portrait' : 'landscape',
      devicetype: Platform.isTablet() ? 'tablet' : 'phone',
    };

    // Event Listener for orientation changes
    Dimensions.addEventListener('change', () => {
      this.setState({
          orientation: Platform.isPortrait() ? 'portrait' : 'landscape'
      });
    });
  
  }

  _onNumber(arg) {
    input = this.state.input === ' ' ? '' : this.state.input;
    input = input + arg;
    this.setState({
      input: input,
    })
  };

  _onStackOp(arg) {
    stack = this.state.stack;
    input = this.state.input
    if (arg == "Drop") {
      stack.pop();
    } else if (arg == "Clear") {
      stack = new Array();
    } else if (arg == "Swap") {
      if (stack.length > 1) {
        arg1 = stack.pop();
        arg2 = stack.pop();
        stack.push(arg1);
        stack.push(arg2);
      }
    } else if (arg == 'Enter') {
      if (input !== ' ') {
        stack.push(input);
        input = ' ';
      }
    }
    this.setState({
      input: input,
      stack: stack,
      output: this.getStackView(stack),
    })
  };

  _onMathOp(arg) {
    if (arg == "+" || arg == "plus") {
      this.applyDoubleArgFunc((a, b) => {return a.plus(b);});
    } else if (arg == "-" || arg == "sub") {
      this.applyDoubleArgFunc((a, b) => {return a.minus(b);});
    } else if (arg == "*" || arg == "mul") {
      this.applyDoubleArgFunc((a, b) => {return a.times(b);});
    } else if (arg == "/" || arg == "div") {
      this.applyDoubleArgFunc((a, b) => {return a.dividedBy(b);});
    }
  };

  applyDoubleArgFunc(func) {
    stack = this.state.stack;
    input = this.state.input;

    if ((input !== ' ' && stack.length < 1) || (input === ' ' && stack.length < 2)) {
      this.setState({
        errorMessage: 'Two arguments are required for this function.',
      });
      return;
    }

    arg2 = input !== ' ' ? input : new BigNumber(stack.pop());
    arg1 = new BigNumber(stack.pop());
    stack.push(String(func(arg1, arg2)));
    this.setState({
      input: ' ',
      stack: stack,
      output: this.getStackView(stack),
    })
  }

  getStackView(stack) {
    let first = true;
    let output = '';

    let x = 0;
    while (x < stack.length) {
      if (!first) {output += "\n";} else {first = false;}
      output += stack[x];
      x++;
    }
    
    return output;
  }

  render() {
    if (!!this.state.errorMessage) {
      return (
        <Overlay isVisible={!!this.state.errorMessage} fullScreen={true}>
          <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
          <Button
            onPress={() => this.setState({errorMessage: false})}
            title="OK, got it" 
            buttonStyle={{
              backgroundColor: "#36578c",
              width: '50%',
              marginLeft: 'auto',
              marginRight: 'auto',
              marginTop: 20,
              borderWidth: 0,
              borderRadius: 15
            }}
            />
        </Overlay>
      );
    } else {
      return (
        <View style={styles.container}>
            <View style={this.state.orientation == 'portrait' ? styles.outputArea : styles.outputAreaLandscape}>
              <Text style={styles.text}>{this.state.output}</Text>
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.text}>{this.state.input}</Text>
            </View>
            <View style={styles.buttonRow}>
              <CalcButton label="Clear" callback={() => this._onStackOp("Clear")} />
              <CalcButton label="Swap"  callback={() => this._onStackOp("Swap")} />
              <CalcButton label="Drop"  callback={() => this._onStackOp("Drop")} />
              <CalcButton label="Enter" callback={() => this._onStackOp("Enter")} />
            </View>
            <View style={styles.buttonRow}>
              <CalcButton label="7" callback={() => this._onNumber("7")} />
              <CalcButton label="8" callback={() => this._onNumber("8")} />
              <CalcButton label="9" callback={() => this._onNumber("9")} />
              <CalcButton label="/" callback={() => this._onMathOp("/")} />
            </View>
            <View style={styles.buttonRow}>
              <CalcButton label="4" callback={() => this._onNumber("4")} />
              <CalcButton label="5" callback={() => this._onNumber("5")} />
              <CalcButton label="6" callback={() => this._onNumber("6")} />
              <CalcButton label="*" callback={() => this._onMathOp("*")} />
            </View>
            <View style={styles.buttonRow}>
              <CalcButton label="1" callback={() => this._onNumber("1")} />
              <CalcButton label="2" callback={() => this._onNumber("2")} />
              <CalcButton label="3" callback={() => this._onNumber("3")} />
              <CalcButton label="-" callback={() => this._onMathOp("-")} />
            </View>
            <View style={styles.buttonRow}>
              <CalcButton label="." callback={() => this._onNumber(".")} />
              <CalcButton label="0" callback={() => this._onNumber("0")} />
              <CalcButton label="=" callback={() => this._onMathOp("=")} />
              <CalcButton label="+" callback={() => this._onMathOp("+")} />
            </View>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
    width: '100%',
  },
  outputArea: {
    width: '95%',
    flex: 5,
    flexDirection: 'column',
    margin: 10,
    justifyContent: 'flex-end',
    borderWidth: 2,
    borderColor: '#36578c',
  },
  outputAreaLandscape: {
    width: '95%',
    flex: 2,
    flexDirection: 'column',
    margin: 10,
    justifyContent: 'flex-end',
    borderWidth: 2,
    borderColor: '#36578c',
  },
  inputContainer: {
    borderWidth: 2,
    borderColor: '#36578c',
    width: '95%',
    marginBottom: 5,
  },
  buttonRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '95%',
    margin: 0,
    padding: 0,
  },
  text: {
    fontSize: 20
  },
  errorMessage: {
    fontSize: 20,
    marginTop: 60,
  }
  
});
